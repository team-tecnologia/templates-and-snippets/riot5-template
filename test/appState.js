import { install } from 'riot'

import store from '../src/store/store'

export const installStates = () => {
    install((c) => {
        c.states$ = store.states$
        c.actions = store.actions
        return c
    })
}

export const getStates = () => store.states$()
export const getActions = () => store.actions

export const clearStates = () => {
    store.resetStates()
    installStates()
}
