/**
 * Using jest version 24.x:
 *  - https://archive.jestjs.io/docs:/en/24.x/getting-started.html
 * Other versions conflict with riot
 * Using riot testing library too:
 *  - https://testing-library.com/docs/ecosystem-riot-testing-library
 *  - https://github.com/ariesjia/riot-testing-library
 * Using transformer to interpret the riot files:
 *  - https://github.com/tompascall/riot-jest-transformer
 * Using a stub transformer to non script files like scss, imgs...
 * To stub more files configure the "transform" key in jest.config.js
 *  - https://github.com/eddyerburgh/jest-transform-stub
 *
 * NOTE: the coverage just work for .js files
 */

import render from 'riot-testing-library'
import { installStates, clearStates } from './appState'

import PgHome from '../src/pages/pg-home.riot'

beforeAll(() => {
    installStates()
})

afterAll(() => {
    clearStates()
})

describe('riot-testing-library', () => {
    it('should render home component and return dom query', () => {
        const { getByText } = render(PgHome)
        expect(getByText('HOME PAGE')).toBeTruthy()
    })
})
