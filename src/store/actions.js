const Actions = (update$, states$) => ({
    actionExample(obj) {
        const updatedObj = {
            ...states$().stateExample,
            ...obj,
        }
        update$({ stateExample: updatedObj })
    },
})

export default Actions
