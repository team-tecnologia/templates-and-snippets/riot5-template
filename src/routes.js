import { resolveRoutes } from './helpers/routes'

/**
 * DON'T add / at the end
 * the path is used on riot/route, is defined based on 'path-to-regexp' module:
 * https://github.com/pillarjs/path-to-regexp
 * */

const routes = [
    {
        path: '',
        component: 'pg-home',
    },
    {
        path: '/home',
        component: 'pg-home',
    },
    {
        path: '/home/:id(\\d+)',
        component: 'pg-home',
    },
]

export default resolveRoutes(routes)
