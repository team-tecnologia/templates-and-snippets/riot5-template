# riot5+meiosis

## '/' Based routes
To use with '/' based routes, your sever must return for all routes the same resources.

E.g.: the route my_site.com and my_site.com/anything must return from server the index.html page,
and than, the index.html will get the bundled js and handle with routes.

For that use, may be necessary add to webpack.configs.js inside 'output' key a ```publicPath: '/'```.

And add '/' at the beginning for all hardcoded resources, like at 'index.html' or 'home.riot' where we use '/media/favicon.png'.

Using all resources with absolute path '/', we can use normally the folders 'font', 'media' ...,
without the server return the index.html page.

The dev server used is already configured to this stuff.

## '#' Based routes
To use with '#' based routes, just keep the project like are current.

This method is required to use with gitlab pages, because gitlab's server don't return 'index.html'
for all paths.

## Getting started

Clone the template

```sh
git clone --depth 1 https://gitlab.com/team-tecnologia/templates-and-snippets/riot5-template project-name
```

Remove old git repo

```sh
cd project-name
rm -rf .git
```

Install plugins

```sh
npm install
```

Run the app

```sh
npm run start
```

Build for production

```sh
npm run build
```
